
def print_hi(name):

    print(f'Hi, {name}')


def commit_check():
    print('This is my second commit')


if __name__ == '__main__':
    print_hi('PyCharm')
    commit_check()

